<?php

$ksiega="ksiega.txt";
if ($_SERVER["REQUEST_METHOD"]=="POST")
{
    $imie=trim(stripslashes(htmlspecialchars($_POST["imie"],ENT_QUOTES)));
    $wpis=stripslashes(htmlspecialchars($_POST["wpis"],ENT_QUOTES));
    if (strlen($wpis))>5 && strlen($wpis) <300 && !empty($imie) {
        $f = fopen($ksiega ,"a");
        $rekord = "imie".n12br($wpis)."\n";

        if (!flock($f, LOCK_EX)){
            fclose($f);
        } else {
            fputs(($f, $rekord));
            flock($f, LOCK_UN);
            fclose($f);
            } 
    
} else {
    echo "<div class=\"error\">wpis lub nick sa za krotkie!!</div>";
}
?>